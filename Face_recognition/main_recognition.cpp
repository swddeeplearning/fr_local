#include "./FaceDetectAlignment_module/FaceDetAlign.hpp"
#include "./CinvokePython_module/CPythonUtil.hpp"
#include "./CinvokePython_module/feature_extract.hpp"
#include <dlib/opencv.h>
#include <opencv2/core/utility.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core_c.h>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>
#include <dlib/string.h>
#include <iostream>
#include <ctime>
#include <sys/time.h>

using namespace dlib;
using namespace std;
using namespace cv;

int main(int argc, char const **argv) 
{

	// Initial web camera
        Mat image;
	//VideoCapture cap(0);
        VideoCapture cap;

        cout << "Try to Open IP Cam ..." << endl;

        cap.open("rtsp://10.60.13.24:554/live.sdp");

        cout << "Open IP Cam Done ..." << endl;

	// Set the camera resolution
	//cap.set(CV_CAP_PROP_FRAME_WIDTH, 640);
	//cap.set(CV_CAP_PROP_FRAME_HEIGHT, 480);

	// Initial feature detection and alignment
	FaceDetectAlign face_processor = FaceDetectAlign("./FaceDetectAlignment_module/shape_predictor_68_face_landmarks.dat");

	// Initial feature extraction
	FeatureExtractor face_recognition = FeatureExtractor("./FeatureExtraction");	
	//FeatureExtractor face_recognition;	
        
        cout << "Loading SubModule is Done ..." << endl;

	// The number of detected face
	int num_face = 0; 


	// Compute performance of detecting face in each frame
	int q_face = 0; 
	struct timeval start, end; 


	// Set the leaving key
	char keycode;

	// The validation results (who is the face in the frame) 	
	string user_name;

	// Grab and process frames until the main window is closed by the user.

	for(;;)
	{
		std::vector<cv::Mat> aligned_image;
		std::vector<cv::Rect> face_rect;
		gettimeofday( &start, NULL );

		// Grab a frame
		cap >> image;

                cout << "image.cols: " << image.cols << endl;
                cout << "image.rows: " << image.rows << endl;
		
                //imshow("Original image", image);
                //waitKey(0);


		// Detect and align face in image to obtain face rectangle and aligned image 
                cout << "Start Face Detecter ... " << endl;
		face_processor.face_detect_align(image, face_rect, aligned_image);
                cout << "Finish Face Detecter ... " << endl;
                
                if(true){

		// Send aligned face to python interface if aligned_image.size() > 0
                if(aligned_image.size() > 0){
		for(unsigned long i = 0; i < aligned_image.size(); i++){
			// Extract feature from aligned_image and obtain the validation result 
                        cout << "Start Face Recognition ... " << endl;
			face_recognition.extract_feature_recognition(aligned_image[i], user_name); 
                        cout << "Finish Face Recognition ... " << endl;

			if (user_name == "Unknown")
			{
				// Drawing rectangles of detecting faces in a frame
				cv::rectangle(image, face_rect[i], Scalar(0, 0, 255), 3, 8, 0);

				// Put the validation result in the upper and left corner of rectangle that includes detected face
				putText(image, user_name, cvPoint(face_rect[i].x, face_rect[i].y), FONT_HERSHEY_TRIPLEX, 2, Scalar(0, 255, 0));
			}
			else{
				// Drawing rectangles of detecting faces in a frame
				cv::rectangle(image, face_rect[i], Scalar(255, 0, 0), 3, 8, 0);

				// Put the validation result in the upper and left corner of rectangle that includes detected face
				putText(image, user_name, cvPoint(face_rect[i].x, face_rect[i].y), FONT_HERSHEY_TRIPLEX, 2, Scalar(0, 255, 0));
			}

			num_face ++;
		}
                }

		if (aligned_image.size() != 0)
		{
			cout << "Each image: the number of detecting face: " << aligned_image.size() << endl;
			//cout << "Total: the count of detecting face: " << num_face << endl;
			cout << "------------------------------------------------" << endl;
		}

		if (aligned_image.size() != 0)
		{
			cout << "The detected user: " << user_name << endl;
			cout << "Each image: the number of detecting face: " << aligned_image.size() << endl;
			//cout << "Total: the count of detecting face: " << num_face << endl;
		}

                }


		imshow("Detected image", image);
		keycode = waitKey(30);

		gettimeofday( &end, NULL ); 
		q_face = 1000000 * ( end.tv_sec - start.tv_sec ) + end.tv_usec - start.tv_usec;
		printf("query face recognition: %d us\n", q_face);
		cout << "------------------------------------------------" << endl;

		if(keycode == 27)
		{
			break;
		}

	}
	return 0;
}

