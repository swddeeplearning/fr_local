#include <dlib/opencv.h>

#include <opencv2/core/utility.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core_c.h>

#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>
#include <dlib/string.h>

#include <iostream>
#include <ctime>

#include "./FaceDetectAlignment_module/FaceDetAlign.hpp"
#include "./CinvokePython_module/CPythonUtil.hpp"
#include "./CinvokePython_module/feature_extract.hpp" 

#include <sys/time.h>

using namespace dlib;
using namespace std;
using namespace cv;

int main(int argc, char* argv[]) 
{
	// Initial feature detection and alignment
	FaceDetectAlign face_processor = FaceDetectAlign("./FaceDetectAlignment_module/shape_predictor_68_face_landmarks.dat");

	// Initial feature extraction
	FeatureExtractor face_recognition = FeatureExtractor("./FeatureExtraction");	

	// Initial web camera
	VideoCapture cap(0);

	// Set the camera resolution
	cap.set(CV_CAP_PROP_FRAME_WIDTH, 1280);
	cap.set(CV_CAP_PROP_FRAME_HEIGHT, 720);

	// The number of detected face
	int num_face = 0; 

	/*
	// Compute performance of detecting face in each frame
	int q_face = 0; 
	struct timeval start, end; 
	*/

	// Set the leaving key
	char keycode;

	// We have three kinds of image
	Mat image;

	// The validation results (who is the face in the frame) 	
	string user_name;

	//*************
	time_t loc_time = 0;
	struct tm *timeinfo;
	char now_time[80];

	string user_faces_dataset = dlib::get_current_dir() + "/face_dataset";
	dlib::create_directory(user_faces_dataset);
	//*************

	// Grab and process frames until the main window is closed by the user.
	for(;;)
	{
		//************************
		time(&loc_time);
		timeinfo = localtime(&loc_time);
		strftime(now_time, 80, "%Y%m%d%H%M%S", timeinfo);
		string system_time(now_time);
		//************************

		std::vector<cv::Mat> aligned_image;
		std::vector<cv::Rect> face_rect;
		//gettimeofday( &start, NULL );

		// Grab a frame
		cap >> image;
		Mat dataset_img = image.clone();

		// Detect and align face in image to obtain face rectangle and aligned image 
		face_processor.face_detect_align(image, face_rect, aligned_image);

		// Send aligned face to python interface if aligned_image.size() > 0
		for(unsigned long i = 0; i < aligned_image.size(); i++){

			// Extract feature from aligned_image and obtain the validation result 
			face_recognition.extract_feature_recognition(aligned_image[i], user_name); 

			if (user_name == "Unknown")
			{
				// Drawing rectangles of detecting faces in a frame
				cv::rectangle(image, face_rect[i], Scalar(0, 0, 255), 3, 8, 0);  

				// Put the validation result in the upper and left corner of rectangle that includes detected face
				putText(image, user_name, cvPoint(face_rect[i].x, face_rect[i].y), FONT_HERSHEY_TRIPLEX, 2, Scalar(0, 255, 0));
			}
			else{
				// Drawing rectangles of detecting faces in a frame
				cv::rectangle(image, face_rect[i], Scalar(255, 0, 0), 3, 8, 0);

				// Put the validation result in the upper and left corner of rectangle that includes detected face
				putText(image, user_name, cvPoint(face_rect[i].x, face_rect[i].y), FONT_HERSHEY_TRIPLEX, 2, Scalar(0, 255, 0));			
			}

			if (!user_name.empty())
			{
				//string user_faces = dlib::get_current_dir() + "/face_dataset/" + user_name;
				string user_faces = user_faces_dataset + "/" + user_name;
				//create user folder
				dlib::create_directory(user_faces);
				//create original folder
				dlib::create_directory(user_faces + "/original_images");
				//create alignment folder
				dlib::create_directory(user_faces + "/aligned_images");
			}

			string filename_template_str1 = "./face_dataset/" + user_name + "/original_images/" + system_time + "-face-XXXXXX.jpg";
			const char* filename_template1 = filename_template_str1.c_str();
			int fd1 = mkstemps(filename_template1, 4);
			imwrite(filename_template1, dataset_img);
			close(fd1);

			cvtColor(aligned_image[i], aligned_image[i], CV_BGR2RGB);

			string filename_template_str2 = "./face_dataset/" + user_name + "/aligned_images/" + system_time + "-face-XXXXXX.jpg";
			const char* filename_template2 = filename_template_str2.c_str();
			int fd2 = mkstemps(filename_template2, 4);
			imwrite(filename_template2, aligned_image[i]);
			close(fd2);


			num_face ++;
		}

		if (aligned_image.size() != 0)
		{
			cout << "Each image: the number of detecting face: " << aligned_image.size() << endl;
			cout << "Total: the count of detecting face: " << num_face << endl;
			cout << "------------------------------------------------" << endl;
		}

		imshow("Detected image", image);

		keycode = waitKey(30);

		if(keycode == 27)
			break;
		/*
		   gettimeofday( &end, NULL ); 
		   q_face = 1000000 * ( end.tv_sec - start.tv_sec ) + end.tv_usec - start.tv_usec;
		   printf("query rgb : %d us\n", q_face);
		   */
	}

	return 0;
}

