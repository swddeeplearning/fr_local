#include <dlib/opencv.h>

#include <opencv2/core/utility.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core_c.h>

#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>
#include <iostream>
#include <dlib/string.h>

using namespace std;
using namespace dlib;
using namespace cv;

#define FRAME_WIDTH	60	
#define FRAME_HEIGHT 60	

class FaceDetectAlign{
	public:
		frontal_face_detector detector;
		shape_predictor pose_model;

		FaceDetectAlign(const char* shape_path);
		void face_detect_align(Mat &original_image, std::vector<cv::Rect> &face_rect, std::vector<cv::Mat> &aligned_image);
		
};
