#include <dlib/opencv.h>

#include <opencv2/core/utility.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core_c.h>

#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>
#include <iostream>
#include <dlib/string.h>

#include "CPythonUtil.hpp"
#include "feature_extract.hpp"

using namespace dlib;
using namespace std;
using namespace cv;

#define FRAME_WIDTH	60	
#define FRAME_HEIGHT 60	

int main(int argc, char const *argv[])
{

	VideoCapture cap(0);
	image_window win1;
	image_window win2;

	// Initial Feature Extraction
	//FeatureExtractor feature_extractor = FeatureExtractor("../");		

	// Load face detection and pose estimation models.
	frontal_face_detector detector = get_frontal_face_detector();
	shape_predictor pose_model;
	deserialize("shape_predictor_68_face_landmarks.dat") >> pose_model;

	int count = 0;
	char keycode;
	string temp;

	// Grab and process frames until the main window is closed by the user.
	for(;;)
	{
		// Grab a frame
		Mat vedio_image;
		cap >> vedio_image;


		/*
		imshow("Look at the camera", vedio_image);
		keycode = waitKey(80);

		if(keycode == 's')
		{
			Mat decided_image;
			decided_image = vedio_image;
			imshow("Decided image", decided_image);

			// Turn OpenCV's Mat into something dlib can deal with. 
			cv_image<bgr_pixel> cimg(decided_image);

			// Detect faces 
			std::vector<dlib::rectangle> faces = detector(cimg);

			if(faces.size() != 0)
			{
				std::vector<full_object_detection> shapes;
				for(unsigned long i = 0; i < faces.size(); ++i)
					shapes.push_back(pose_model(cimg, faces[i]));

				// Now let's view our face poses on the screen.
				win1.clear_overlay();
				win1.set_image(cimg);
				win1.add_overlay(faces);
				win1.add_overlay(render_face_detections(shapes));

				// We can also extract copies of each face that are cropped, rotated upright,
				// and scaled to a standard size as shown here:
				dlib::array<array2d<rgb_pixel> > face_chips;
				extract_image_chips(cimg, get_face_chip_details(shapes, 60), face_chips);
				win2.set_image(tile_images(face_chips));

				// Transform image format of Dlib to image format OpenCV
				Mat aligned_image;
				aligned_image = toMat(face_chips[0]);

				feature_extractor.extract_feature_recognition(aligned_image, temp);
				//showImage(pyObj, aligned_image);
				count ++;
			}
		}
		if(keycode == 27)
			break;
	*/
	}
	
}










