#include <dlib/opencv.h>

#include <opencv2/core/utility.hpp>                                         
#include <opencv2/highgui/highgui.hpp>                                      
#include <opencv2/imgproc/imgproc.hpp>                                      
#include <opencv2/core/core_c.h>                                            
                                                                            
#include <dlib/image_processing/frontal_face_detector.h>                    
#include <dlib/image_processing/render_face_detections.h>                   
#include <dlib/image_processing.h>                                          
#include <dlib/gui_widgets.h>                                               
#include <dlib/image_io.h>                                                  
#include <dlib/string.h>                                                    
                                                                            
#include <iostream>

#include "CPythonUtil.hpp"

using namespace cv;
using namespace std;

class FeatureExtractor {
public:
    FeatureExtractor(const char* feature_extract_path);
    void extract_feature_recognition(const Mat& frame, string& temp);
    ~FeatureExtractor();

//private:
    //Initial Python Interface
    CinvokePythonUtil *pyObj;
};
