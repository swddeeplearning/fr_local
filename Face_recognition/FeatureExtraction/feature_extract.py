import sklearn
import numpy as np

import matplotlib.pyplot as plt
import skimage
import sys
sys.path.insert(0, '/home/facerecognition/caffe/python')
import caffe

import sklearn.metrics.pairwise as pw

import cv2
import os.path as osp

from face_database import Face_Database


ROOT_DIR = osp.split(osp.realpath(__file__))[0]

class Feature_Extract(object):
    #_fast_rcnn = None;
    def __init__(self):
        caffe.set_mode_cpu()
        
        caffe_mean = osp.join(ROOT_DIR, 'caffe_model/mean.binaryproto')
        caffe_deploy_ip = osp.join(ROOT_DIR, 'caffe_model/deploy_ip.prototxt')
        caffe_model = osp.join(ROOT_DIR, 'caffe_model/snapshot_iter_805250.caffemodel')
        
        self.MEAN_FILE = caffe_mean
        self.net_ip = caffe.Classifier(caffe_deploy_ip, caffe_model)
       
    def image_subtract_mean(self, frame):
        mean_blob = caffe.proto.caffe_pb2.BlobProto()
        mean_blob.ParseFromString(open(self.MEAN_FILE, 'rb').read())
        mean_arr = np.array(caffe.io.blobproto_to_array(mean_blob))
        mean_arr = mean_arr[0]
        mean_arr /= 255 
        frame -= mean_arr

        return frame

    def image_extract_feature(self, frame):
        X = np.empty((1, 3, 60, 60))
        
        if frame.ndim < 3:
            print 'gray:'+filename
            X[0, 2, :, :] = frame[:, :]
            X[0, 1, :, :] = frame[:, :] 
            X[0, 0, :, :] = frame[:, :] 
        else:
            X[0, 2, :, :] = frame[2, :, :]
            X[0, 1, :, :] = frame[1, :, :]
            X[0, 0, :, :] = frame[0, :, :]
            
        out_ip = self.net_ip.forward_all(data = X)
        feature = np.float64(out_ip['ip'])
        feature = np.reshape(feature,(1, 160))

        return feature
    
        







