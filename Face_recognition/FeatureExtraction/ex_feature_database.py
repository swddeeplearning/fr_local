import string
import sklearn
import numpy as np

import matplotlib.pyplot as plt
import skimage
import sys
sys.path.insert(0, '/home/facerecognition/caffe/python')
import caffe

import sklearn.metrics.pairwise as pw

import cv2
import os.path as osp

from face_database import Face_Database
from feature_extract import Feature_Extract

class SaveFeatureToDatabase():
    im_process = None

    def sendImg(self, frame, height, width, channels):
        #print type(frame)
        frame = np.reshape(frame, (height, width, channels))
        frame = frame.astype(np.float64)
        frame /= 255
        
        # bgr rgb ? toMat
        frame = frame[:,:,(2,1,0)] 
        frame = frame.transpose((2,0,1))
        
        if self.im_process is None:
            self.im_process = Feature_Extract()

        
        #image subtract mean value
        frame = self.im_process.image_subtract_mean(frame)
        
        #extract image feature
        feature = self.im_process.image_extract_feature(frame)
        feature = feature.reshape(1, -1)
              
        ####################################################
        #    Store and obtain the feature into Database    #
        ####################################################

        #Create DB Object
        face_db = Face_Database()
        user_name = ' '
        min_mt = 0.50
        #Store feature to sqlite3 BLOB
        #face_db.register_face_data('user_004', feature)
        
        #Obtain feature from sqlite3 BLOB
        feature_buffers = face_db.select_face_data()

        for feature_buffer in feature_buffers:
            select_feature = np.frombuffer(feature_buffer[1], dtype=np.float64)
            select_feature = select_feature.reshape(1, -1)
            mt = pw.pairwise_distances(feature, select_feature, metric = 'cosine')
            if mt < 0.50:
                curr_mt = mt   
                if curr_mt <= min_mt:
                    min_mt = curr_mt
                    user_name = feature_buffer[0]
                    print 'mt = ', mt

        if not user_name.strip():
            user_name = 'Unknown'
         
        return user_name,
        


