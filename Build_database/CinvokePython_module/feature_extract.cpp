#include <dlib/opencv.h>

#include <opencv2/core/utility.hpp>                                         
#include <opencv2/highgui/highgui.hpp>                                      
#include <opencv2/imgproc/imgproc.hpp>                                      
#include <opencv2/core/core_c.h>                                            
                                                                            
#include <dlib/image_processing/frontal_face_detector.h>                    
#include <dlib/image_processing/render_face_detections.h>                   
#include <dlib/image_processing.h>                                          
#include <dlib/gui_widgets.h>                                               
#include <dlib/image_io.h>                                                  
#include <dlib/string.h>                                                    

#include <iostream>

#include "CPythonUtil.hpp"
#include "feature_extract.hpp"

#define FRAME_WIDTH 60
#define FRAME_HEIGHT 60

using namespace dlib; 
using namespace cv;
using namespace std;


FeatureExtractor::FeatureExtractor(const char* feature_extract_path){
	//Initial Python Interface
	pyObj = new CinvokePythonUtil("ex_feature_database", "SaveFeatureToDatabase", feature_extract_path);
}

void FeatureExtractor::extract_feature_recognition(const Mat& frame, char* user_name){
	
	pyObj->storeResult(pyObj->callMethod(pyObj->getInstance(),
				"sendImg",
				"(O, i, i, i, s)", 
				pyObj->createPyArray(frame.ptr(0), FRAME_HEIGHT, FRAME_WIDTH, 3), FRAME_HEIGHT, FRAME_WIDTH, 3, user_name));// send image modify
}

FeatureExtractor::~FeatureExtractor(){
	//delete pyObj;
}


