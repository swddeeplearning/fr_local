#include <dlib/opencv.h>

#include <opencv2/core/utility.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core_c.h>

#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>
#include <dlib/string.h>

#include <iostream>
#include <ctime>

#include "./FaceDetectAlignment_module/FaceDetAlign.hpp"
#include "./CinvokePython_module/CPythonUtil.hpp"
#include "./CinvokePython_module/feature_extract.hpp" 

#include <sys/time.h>

using namespace dlib;
using namespace std;
using namespace cv;

int main(int argc, char* argv[]) 
{

	if(argc <= 1){
		cout << "Please input user's ID in database !!" << endl;
		cout << "usage: ./run_build_database ID" << endl;
		return -1;
	}


	// Initial feature detection and alignment
	FaceDetectAlign face_processor = FaceDetectAlign("./FaceDetectAlignment_module/shape_predictor_68_face_landmarks.dat");

	// Initial feature extraction
	FeatureExtractor face_recognition = FeatureExtractor("./FeatureExtraction");	
	FeatureExtractor face_database = FeatureExtractor("./FeatureExtraction");

	// Initial web camera
	//VideoCapture cap(0);
	VideoCapture cap;
	// IP cam	
	cap.open("rtsp://10.60.13.24:554/live.sdp");
	// Set the camera resolution
	//cap.set(CV_CAP_PROP_FRAME_WIDTH, 1280);
	//cap.set(CV_CAP_PROP_FRAME_HEIGHT, 720);

	// The number of detected face
	int num_face = 0; 

	// Set the leaving key
	char keycode;

	// We have three kinds of image
	Mat image;

	// The validation results (who is the face in the frame) 	
	char* user_name = argv[1];

	//*************
	time_t loc_time = 0;
	struct tm *timeinfo;
	char now_time[80];

	string user_faces_dataset = dlib::get_current_dir() + "/face_dataset";
	dlib::create_directory(user_faces_dataset);
	//*************

	// Grab and process frames until the main window is closed by the user.
	for(;;)
	{
		//************************
		time(&loc_time);
		timeinfo = localtime(&loc_time);
		strftime(now_time, 80, "%Y%m%d%H%M%S", timeinfo);
		string system_time(now_time);
		//************************

		std::vector<cv::Mat> aligned_image;
		std::vector<cv::Rect> face_rect;
		//gettimeofday( &start, NULL );

		// Grab a frame
		cap >> image;
		Mat dataset_img = image.clone();

		// Detect and align face in image to obtain face rectangle and aligned image 
		face_processor.face_detect_align(image, face_rect, aligned_image);

		// Send aligned face to python interface if aligned_image.size() > 0
		for(unsigned long i = 0; i < aligned_image.size(); i++){

			// Drawing rectangles of detecting faces in a frame
			cv::rectangle(image, face_rect[i], Scalar(0, 0, 255), 3, 8, 0);  

			char c = waitKey(80);
			if(c == 's')
			{
				
				// Using user_name to build database
				face_recognition.extract_feature_recognition(aligned_image[i], user_name);

				string user_name_string(user_name);				
				if (!user_name_string.empty())
				{
					//string user_faces = dlib::get_current_dir() + "/face_dataset/" + user_name_string;
					string user_faces = user_faces_dataset + "/" + user_name_string;
					//create user folder
					dlib::create_directory(user_faces);
					//create original folder
					dlib::create_directory(user_faces + "/original_images");
					//create alignment folder
					dlib::create_directory(user_faces + "/aligned_images");
				}

				string filename_template_str1 = "./face_dataset/" + user_name_string + "/original_images/" + system_time + "-face-XXXXXX.jpg";
				const char* filename_template1 = filename_template_str1.c_str();
				int fd1 = mkstemps(filename_template1, 4);
				imwrite(filename_template1, dataset_img);
				close(fd1);

				cvtColor(aligned_image[i], aligned_image[i], CV_BGR2RGB);

				string filename_template_str2 = "./face_dataset/" + user_name_string + "/aligned_images/" + system_time + "-face-XXXXXX.jpg";
				const char* filename_template2 = filename_template_str2.c_str();
				int fd2 = mkstemps(filename_template2, 4);
				imwrite(filename_template2, aligned_image[i]);
				close(fd2);

				imshow("Register image", aligned_image[i]);
				waitKey(80);
			}

			num_face ++;
		}

		imshow("Detected image", image);

		keycode = waitKey(80);
		if(keycode == 27)
			break;
	}

	return 0;
}

