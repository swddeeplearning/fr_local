import sys, os
import os.path as osp
from os.path import basename
import numpy as np

import cv2
import skimage
import skimage.io as skio
from skimage.transform import resize as imresize

import matplotlib.pyplot as plt

import sqlite3 as sqldb

import glob

#Define ROOT DIR
ROOT_DIR = osp.split(osp.realpath(__file__))[0]

class Face_Database(object):
    def __init__(self):
        #initial Database
        self.initial_face_database()
        

    def get_db_connection(self):
        #Reture Database connection
        db_conn = osp.join(ROOT_DIR, '../../Face_recognition/FeatureExtraction/feature.db')
        return sqldb.connect(db_conn)

    def execute_sql_without_result(self, execute_sql, data=None):
        #Create database get Operate object
        with self.get_db_connection() as conn:
            cur = conn.cursor()
            
            #Execute SQL
            if data is None:
                cur.execute(execute_sql)
            else:
                cur.execute(execute_sql, data)
            
            #Commit to DB
            conn.commit()

            #Close Cursor
            cur.close()

    def execute_sql_with_result(self, execute_sql, data=None):
        #Result data
        result = None
        
        #Debug
        #print execute_sql
        #print data

        #Create database get Operate object
        with self.get_db_connection() as conn:
            cur = conn.cursor()
            
            #Execute SQL
            if data is None:
                cur.execute(execute_sql)
            else:
                cur.execute(execute_sql, data)
            
            #Commit to DB
            #conn.commit()

            #Get data from Cursor
            result = cur.fetchall()
            
            #Debug
            #print result

            #Close Cursor
            cur.close()

        #Reture result
        return result

    def initial_face_database(self):
        #Create Table for Feature
        create_table =  " CREATE TABLE IF NOT EXISTS "
        create_table += " face_data "
        create_table += " (UserName TEXT, Feature BLOB) "
        #Execute sql
        self.execute_sql_without_result(create_table)
        

    def insert_face_data(self, user_name, face_feature):
        #Insert Face data to Table
        insert_face_data = " INSERT INTO face_data VALUES(?, ?) "

        #Create face data for db
        face_data = (user_name, buffer(face_feature))

        #Execute sql
        self.execute_sql_without_result(insert_face_data, face_data)

    def select_face_data(self, user_name=None):
        #Result data
        result = None
        
        #Check if user_name is None then return all face data
        #      else return specific face data
        if user_name is None:
            #Select all face data from table
            select_face_data = " SELECT UserName, Feature FROM face_data WHERE 1=1 "

            #Execute sql
            result = self.execute_sql_with_result(select_face_data)

        else:
            #Select face data from table by user_name
            select_face_data = " SELECT UserName, Feature FROM face_data WHERE UserName = ? "

            select_condition = (user_name,)

            #Execute sql
            result = self.execute_sql_with_result(select_face_data, select_condition)

        '''
        #result information
        print 'select face data type: {}'.format(type(result))
        print 'select face data: {}'.format(result)
        #print 'select face data -> type: {} buffer: {}'.format(type(result[0]), result[0])
        #print 'select face data -> type: {} buffer: {}'.format(type(result[0][0]), result[0])
        '''

        #Return result
        if not result:
            #if List is empty return False
            return False
        else:
            return result
    
    def register_face_data(self, user_name, face_feature):
        #Check User already exists or not
        if self.select_face_data(user_name):
            '''
            #User already exists
            print 'Register User {} already exists !!'.format(user_name)
            '''
            return False
        else:
            #if User does not exist then insert it to database
            #face_feature is a 160-D or 256-D numpy.ndarray
            self.insert_face_data(user_name, face_feature)

        return True

"""
Class Unit Test Code
"""
def unit_test():
    #Create DB Object
    face_db = Face_Database()

    #Read Image
    image = skio.imread("./girl001.jpg", as_grey=False)
    
    #Resize Image to 100 x 100
    image = imresize(image, (100, 100), preserve_range=True)
    image = image.astype(np.uint8)
    
    #Show Information
    print 'image data structure: {}'.format(type(image))
    print 'image shape: {}'.format(image.shape)
    print 'image dtype: {}'.format(image.dtype)

    #Store image to sqlite3 BLOB
    face_db.register_face_data('girl001', image)

    #Get image buffer from sqlite3 BLOB
    image_buffer = face_db.select_face_data("girl001")[0][1]
    
    #Convert Buffer to Image(ndarray)
    select_image = np.frombuffer(image_buffer, dtype=np.uint8)
    
    print 'before reshape, select image data structure: {}'.format(type(select_image))
    print 'before reshape, select image shape: {}'.format(select_image.shape)
    print 'before reshape, select image dtype: {}'.format(select_image.dtype)
    
    select_image = select_image.reshape((100, 100, 3))
    
    print 'after reshape, select image data structure: {}'.format(type(select_image))
    print 'after reshape, select image shape: {}'.format(select_image.shape)
    print 'after reshape, select image dtype: {}'.format(select_image.dtype)
    
    #Show Selected Image
    skio.imshow(select_image)
    skio.show()


'''
def unit_test_multiple():
    #Create DB Object
    face_db = Face_Database()

    #Get all Image data
    image_list = glob.glob("*.jpg")
    print 'image_list type: {} content: {}'.format(type(image_list), image_list)
    
    for image_name in image_list:
        #Read Image
        image = skio.imread(image_name, as_grey=False)
    
        #Resize Image to 100 x 100
        image = imresize(image, (100, 100), preserve_range=True)
        image = image.astype(np.uint8)

        #Show Information
        print 'image data structure: {}'.format(type(image))
        print 'image shape: {}'.format(image.shape)
        print 'image dtype: {}'.format(image.dtype)
        print 'image name: {}'.format(basename(osp.splitext(image_name)[0]))
        #print image
        
        #Store image to sqlite3 BLOB
        face_db.register_face_data(basename(osp.splitext(image_name)[0]), image)

    
    #Get all image buffer from sqlite3 BLOB
    images_buffer = face_db.select_face_data()

    for image_buffer in images_buffer:
        #Convert Buffer to Image(ndarray)
        select_image = np.frombuffer(image_buffer[1], dtype=np.uint8)
        select_image = select_image.reshape((100, 100, 3))
        
        print 'select image data structure: {}'.format(type(select_image))
        print 'select image shape: {}'.format(select_image.shape)
        print 'select image dtype: {}'.format(select_image.dtype)
    
        #Show Selected Image
        skio.imshow(select_image)
        skio.show()
    
    print 'It\'s Done !!'
'''

if __name__ == '__main__':
    #Unit Test cod
    print 'Run unit_test() ...'
    unit_test()
    '''
    print 'Run unit_test_multiple() ...'
    unit_test_multiple()
    '''
