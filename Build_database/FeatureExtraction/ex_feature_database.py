import string
import sklearn
import numpy as np

import matplotlib.pyplot as plt
import skimage
import sys
sys.path.insert(0, '/home/facerecognition/caffe/python')
import caffe

import sklearn.metrics.pairwise as pw

import cv2
import os.path as osp

from face_database import Face_Database
from feature_extract import Feature_Extract

class SaveFeatureToDatabase():
    im_process = None

    def sendImg(self, frame, height, width, channels, user_name):
        #print type(frame)
        frame = np.reshape(frame, (height, width, channels))
        frame = frame.astype(np.float64)
        frame /= 255
        
        # bgr rgb ? toMat
        frame = frame[:,:,(2,1,0)] 
        frame = frame.transpose((2,0,1))
        
        if self.im_process is None:
            self.im_process = Feature_Extract()

        
        #image subtract mean value
        frame = self.im_process.image_subtract_mean(frame)
        
        #extract image feature
        feature = self.im_process.image_extract_feature(frame)
        feature = feature.reshape(1, -1)

        ####################################################
        #    Store and obtain the feature into Database    #
        ####################################################

        #Create DB Object
        face_db = Face_Database()

        #Store image to sqlite3 BLOB
        face_db.register_face_data(user_name, feature)


