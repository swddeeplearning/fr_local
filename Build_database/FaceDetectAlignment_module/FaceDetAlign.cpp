#include "FaceDetAlign.hpp"

using namespace cv;
using namespace std;

// Initial face detection and face alignment
FaceDetectAlign::FaceDetectAlign(const char* shape_path)
{
	detector = get_frontal_face_detector();
	deserialize(shape_path) >> pose_model;
}

// Detect and align face in image to obtain face rectangle and aligned image
void FaceDetectAlign::face_detect_align(Mat &image, std::vector<cv::Rect> &face_rect, std::vector<cv::Mat> &aligned_image)
{

	// CV image format -> Dlib image format
	Mat detect_image = image;
	
	//cv_image<bgr_pixel> cimg(detect_image);
	cv_image<bgr_pixel> cimg(detect_image);


	// Using detector to detect faces 
	std::vector<dlib::rectangle> faces = detector(cimg);
	
	if(faces.size() != 0)
	{
		std::vector<full_object_detection> shapes;

		for(unsigned long i = 0; i < faces.size(); ++i){  
			// Push back the detected faces into shapes
			shapes.push_back(pose_model(cimg, faces[i]));
			
			// Push back the location includes face into face_rect
			face_rect.push_back(Rect(faces[i].left(), faces[i].top(), faces[i].right()-faces[i].left(), faces[i].bottom()-faces[i].top()));
		}

		// We can also extract copies of each face that are cropped, rotated upright, and scaled to a standard size as shown here:
		// Note that, the format of "face_chips" is "rgb"
		dlib::array< array2d<rgb_pixel> > face_chips;
		
		// The size of aligned image is 60x60
		extract_image_chips(cimg, get_face_chip_details(shapes, 60), face_chips);
		
		// Dlib image format -> CV image format and push back face_chips into aligned_image
		for(unsigned long i = 0; i < face_chips.size(); ++i){
			aligned_image.push_back(toMat(face_chips[i]).clone());
			//imshow("Aligned image", aligned_image[i]); // RGB format
		}
	}
}
